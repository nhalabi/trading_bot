# trading_bot

To run bot do this:

`
cd lib;
python3 trade.py MAC Kraken XXBTZEUR 0.012 ../keys/kraken.key 20 14029 short;
`

* MAC: is the trading strategy class name.
* Kraken: is the exchange adaptor name.
* XXBTZEUR: is the pair to trade. Its format depends on the adaptor being used.
* 0.012: is the volume you intend to trade in each buy/sell.
* ../keys/kraken.key: your key for the exchange.
* 20: Frequency of checking market in seconds.
* 14029: A seed price. Just check current price and shuv it in.
* short: The current state of the bot. short means you are out of the market and you gonna buy next (long) when appropriate.

# Settings

* No settings are available now. To change shorting and longing thresholds, edit lib/trade.py