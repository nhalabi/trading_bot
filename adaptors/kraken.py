import krakenex
import time
import urllib.request
import traceback
import json

class Kraken():
	def __init__(self, params):
		self.API = krakenex.API()
		self.API.load_key(params['key_file'])
		self.buy_orders = []
		self.sell_orders = []
		self.pair = params['pair']
		self.short_pair = self.pair[1:4] + self.pair[5:8]
		self.volume = params['volume']
		self.state = params['state']
		
	def cancel_orders(self, type, open_orders):
		"""Cancel all open orders of type <type>"""
		for key, order in open_orders.items():
			cur_type = order['descr']['type']
			cur_pair = order['descr']['pair']
			if cur_type == type and (cur_pair == self.pair or \
			   (len(cur_pair) == 6 and self.short_pair == cur_pair)):
				try:
					self.API.query_private('CancelOrder', {'txid': key})
				except Exception as ex:
					print('Could not ensure cancel order {}'.format(key))
				finally:
					time.sleep(1)

	def baught(self, closed_orders):
		"""Have we baught into the market?"""
		
		res = True
		last_closed = ''
		closedtm = -1
		
		for key, order in closed_orders.items():
			if 'closetm' in order and float(order['closetm']) > closedtm and \
			   (order['descr']['pair'] == self.pair or \
			   order['descr']['pair'] == self.short_pair):
				closedtm = int(order['closetm'])
				last_closed = key
		
		if closedtm == -1:
			return True
		
		order = closed_orders[last_closed]
		if order['descr']['type'] == 'sell':
			res = False
		
		return res
	
	def buying(self, open_orders):
		"""Do we have an open order to buy?"""
		
		for key, order in open_orders.items():
			if order['descr']['type'] == 'buy':
				return True
		
		return False
		
	def sold(self, closed_orders):
		"""Have we sold out of the market?"""
		
		res = True
		last_closed = ''
		closedtm = -1
		
		for key, order in closed_orders.items():
			if 'closetm' in order and float(order['closetm']) > closedtm and \
			   (order['descr']['pair'] == self.pair or \
			   order['descr']['pair'] == self.short_pair):
				closedtm = float(order['closetm'])
				last_closed = key
		
		if closedtm == -1:
			return True
		order = closed_orders[last_closed]
		
		if order['descr']['type'] == 'buy':
			res = False
			
		return res
		
	def selling(self, open_orders):
		"""Do we have an open order to sell out of market?"""
		
		for key, order in open_orders.items():
			if order['descr']['type'] == 'sell':
				return True
		
		return False
	
	def add_order(self, type):
		try:
			self.API.query_private('AddOrder', {'pair': self.pair,
												'type': type,
												'ordertype': 'market',
												'volume': str(self.volume)})
			time.sleep(1)
		except Exception as ex:
			print('Order did not return confirmation')
	
	def cancel_duplicates(self, open_orders):
		"""Cancel all same type open orders (sell/buy)"""
		
		types = set()
		to_cancel = {}
		
		for key, order in open_orders.items():
			if order['descr']['type'] in types:
				to_cancel[key] = order
			types.add(order['descr']['type'])
		
		for type in types:
			self.cancel_orders(type, to_cancel)
	
	def get_orders(self):
		open_orders = {}
		closed_orders = {}
		
		try:
			# Get orders
			open_orders = self.API.query_private('OpenOrders', {'docalcs': 'true'})
			open_orders = open_orders['result']['open']
			time.sleep(1)
			closed_orders = self.API.query_private('ClosedOrders', {'docalcs': 'true'})
			closed_orders = closed_orders['result']['closed']
			time.sleep(1)
			
			# Clean data
			to_del = []
			for key, order in open_orders.items():
				if not 'descr' in order or not 'type' in order['descr'] or \
				   not 'pair' in order['descr']:
					to_del.append(key)
			for key in to_del:
				del open_orders[key]
			to_del = []
			for key, order in closed_orders.items():
				if not 'descr' in order or not 'type' in order['descr'] or \
				   not 'pair' in order['descr']:
					to_del.append(key)
			for key in to_del:
				del closed_orders[key]
				
		except Exception as ex:
			print('Could not get order info')
			traceback.print_exc()
			
		return open_orders, closed_orders
	
	def buy(self):
		open_orders, closed_orders = self.get_orders()
		self.cancel_duplicates(open_orders)
		self.cancel_orders('sell', open_orders)
		if self.state == 'sell':
			self.state = 'buy'
			self.add_order('buy')
		elif self.state == 'buy':
			if not self.baught(closed_orders) and \
			   not self.buying(open_orders):	
				print('Algo did not buy the first time, trying again')
				self.add_order('buy')
			else:
				self.cancel_orders('buy', open_orders)
		
	def sell(self):
		open_orders, closed_orders = self.get_orders()
		self.cancel_duplicates(open_orders)
		self.cancel_orders('buy', open_orders)
		if self.state == 'buy':
			self.state = 'sell'
			self.add_order('sell')
		elif self.state == 'sell':
			if not self.sold(closed_orders) and \
			   not self.selling(open_orders):
				print('Algo did not sell the first time, trying again')	
				self.add_order('sell')
			else:
				self.cancel_orders('sell', open_orders)
	
	def get_price(self):
		try:
			r = urllib.request.urlopen('https://min-api.cryptocompare.com/data/price?fsym=BTC&tsyms=EUR')
			data = json.loads(r.read().decode(r.info().get_param('charset') or 'utf-8'))
			print('current price from cryptocompare is {} EUR'.format(data['EUR']))
			#return float(data['EUR'])
		except Exception as ex:
			print('Could not get price from cryptocompare')
			traceback.print_exc()
			
		try:
			price = self.API.query_public('Ticker', {'pair': self.pair})['result'][self.pair]
			AVG = ((float(price['a'][0]) * float(price['a'][1])) + (float(price['b'][0]) * float(price['b'][1]))) / (float(price['a'][1]) + float(price['b'][1]))
			LC = price['c'][0]
			print('current price from kraken is last closed: {} average: {} EUR'.format(LC, AVG))
			time.sleep(1)
			return float(LC)
		except Exception as ex:
			print('Could not get price from kraken')
			traceback.print_exc()
		
		return None
