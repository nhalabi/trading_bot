import time
import importlib
import sys
import traceback
sys.path.append('..')
from strategies import *
from adaptors import *

# Setup adaptor
adapt_class = getattr(locals()[sys.argv[2].lower()], sys.argv[2])
state = 'sell'
if sys.argv[8] == 'long':
	state = 'buy'
adaptor = adapt_class({
	'pair': sys.argv[3],
	'volume': sys.argv[4],
	'key_file': sys.argv[5],
	'state': state
})

# Setup strategy
strat_class = getattr(locals()[sys.argv[1].lower()], sys.argv[1])
strat = strat_class(adaptor)
strat.update_params({
	'slow': 30,
	'fast': 10,
	'short': 1,
	'long': 1,
	'state': sys.argv[8],
	'seed_price': float(sys.argv[7]),
})
secs = int(sys.argv[6])

# Start Trading
while(True):
	try:
		strat.update()
		strat.decide()
		strat.commit()
	except Exception as ex:
		print(ex)
		traceback.print_exc()
		
	time.sleep(secs)
