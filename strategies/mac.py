class MAC:
	def __init__(self, adaptor):
		self.adaptor = adaptor # API
		self.trend = 0 # Current trend indicator
		self.state = 'short' # Current decision
		
	def update_params(self, params):
		self.slow = params['slow'] # Slow Moving Avg size
		self.fast = params['fast'] # FastMoving Avg size
		self.short = params['short'] # Short thresh
		self.long = params['long'] # Long thresh
		cur_price = params['seed_price'] # Initial Price is manual
		self.state = params['state'] # Initial Price is manual
		self.slow_ma = [cur_price] * self.slow # Initiate SMA
		self.fast_ma = [cur_price] * self.fast # Initiate FMA
		
	def avg(self, vals):
		return sum(vals) / len(vals)
	
	def exp_avg(self, vals, t, a=0.1):
		if t == 0:
			return vals[t]
		else:
			return a * vals[t] + (1 - a) * self.exp_avg(vals, t - 1, a)

	def update(self):
		"""Update current state. Moving Avgs and trend"""
		
		cur_price = self.adaptor.get_price()
		if not cur_price is None:
			self.slow_ma = self.slow_ma[1:] + [cur_price]
			self.fast_ma = self.fast_ma[1:] + [cur_price]
		else:	
			self.slow_ma = self.slow_ma[1:] + [self.slow_ma[-1]]
			self.fast_ma = self.fast_ma[1:] + [self.fast_ma[-1]]
		#diff = self.exp_avg(self.fast_ma, len(self.fast_ma) - 1) - \
		#       self.exp_avg(self.slow_ma, len(self.slow_ma) - 1)
		diff = self.avg(self.fast_ma) - self.avg(self.slow_ma)

		if self.trend * diff >= 0:
			self.trend += diff
		else:
			self.trend = diff
		
		cur_trend = 100 * self.trend / self.slow_ma[-1]
		
		print('cur trend {}'.format(cur_trend))
		
	def decide(self):
		"""Decide whether to buy or sell"""
		
		cur_trend = 100 * self.trend / self.slow_ma[-1]	
		if cur_trend > self.long:
			print('longing with trend {}'.format(cur_trend))
			self.state = 'long'
		if cur_trend < -self.short:
			print('shorting with trend {}'.format(cur_trend))
			self.state = 'short'
			
	def commit(self):
		"""Buy or sell after checking current position and open orders"""
		
		if self.state == 'long':
			self.adaptor.buy()
		if self.state == 'short':
			self.adaptor.sell()
				
